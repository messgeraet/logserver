"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        test_sensor.py


    FIRST RELEASE
        2017-08-12  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import base64
from datetime import datetime, timedelta

import jwt

from sqlalchemy.exc import IntegrityError

from app import db
from app.tokenutils import generate_token
from app.models import User, Token

SENSOR_EP = '/api/v1/sensor'
SENSORGETLIST_EP = SENSOR_EP + '/{sensor}'
SENSORGETVALUE_EP = SENSORGETLIST_EP + '/{timestamp}'

TOKEN_EP = '/api/v1/token'

# pylint: disable=C0103,W0613,C0111


def get_token(hwid='HWID', ids=None, mtds=None, exp=2, user=None, test=False,
              uni=False):
    if ids is None:
        ids = ['test1', 'test2', 'test3']
    if mtds is None:
        mtds = ['GET', 'POST']
    if user is None:
        user = User.query.filter_by(username='testuser').first()
    token = generate_token(hwid, ids, mtds, exp, user, test, uni)
    data = {'token': token}
    dbtoken = Token.create(data)
    try:
        db.session.add(dbtoken)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
    return [('Authorization', 'Bearer ' + token)]


def get_auth_header(username, password):
    token = base64.b64encode((username + ':' + password).encode('ascii'))
    # headers is a list of tuples not a dict!!!!
    return [('Authorization', 'Basic ' + token.decode('ascii'))]


def check_token_entry(token, payload, key, user=None):
    """check entry"""
    decoded_token = jwt.decode(token, verify=False)

    if key not in payload and key != 'iss':
        raise KeyError('Wrong key: %s' % key)

    if key == 'iss':
        if user is None:
            raise ValueError('user not set')
        assert decoded_token[key] == user
    elif isinstance(payload[key], list):
        assert key in decoded_token
        for entry in payload[key]:
            assert entry in decoded_token[key]
    elif key == 'exp' and payload[key] > 0:
        assert key in decoded_token
        timestamp = datetime.now() + timedelta(hours=payload['exp'])
        assert abs(decoded_token[key] - timestamp.timestamp()) < 300
    elif key == 'exp' and payload[key] <= 0:
        assert key not in decoded_token
    else:
        assert decoded_token[key] == payload[key]


def get_payload(hwid='HARDWAREID', ids=None, mtds=None, exp=2, uni=False):
    if ids is None:
        ids = ['test1', 'test2']
    if mtds is None:
        mtds = ['GET', 'POST']
    data = {
        'hwid': hwid,
        'ids': ids,
        'mtds': mtds,
        'exp': exp
    }
    if uni:
        data['uni'] = True
    return data


# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
