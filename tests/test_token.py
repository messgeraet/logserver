"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        test_token.py

    FIRST RELEASE
        2017-07-27  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import pytest  # pylint:disable=W0611
import jwt

from utils import get_auth_header, check_token_entry, get_payload, TOKEN_EP

# pylint: disable=C0103,W0613,C0111


def test_all_methodes_no_password(client):
    assert client.delete(TOKEN_EP).status_code == 403
    assert client.get(TOKEN_EP).status_code == 403
    assert client.patch(TOKEN_EP).status_code == 403
    assert client.post(TOKEN_EP).status_code == 403
    assert client.put(TOKEN_EP).status_code == 403


def test_all_methodes_with_password(client):
    """Check baseauth"""
    # Check with known user
    user = 'testuser'
    headers = get_auth_header(user, 'PASSWORD')
    assert client.delete(TOKEN_EP, headers=headers).status_code == 405
    assert client.get(TOKEN_EP, headers=headers).status_code == 405
    assert client.patch(TOKEN_EP, headers=headers).status_code == 405
    # POST is allowed method but the payload is missig
    assert client.post(TOKEN_EP, headers=headers).status_code == 400
    assert client.put(TOKEN_EP, headers=headers).status_code == 405


@pytest.mark.parametrize('user', ['testuser', 'testadminuser'])
def test_request_a_token_with_exp(client, user):
    headers = get_auth_header(user, 'PASSWORD')
    payload = get_payload()

    response = client.post(TOKEN_EP, data=payload, headers=headers)

    assert response.status_code == 201
    assert 'token' in response.json
    token = response.json['token']

    check_token_entry(token, payload, 'hwid')
    check_token_entry(token, payload, 'ids')
    check_token_entry(token, payload, 'mtds')
    check_token_entry(token, payload, 'exp')
    check_token_entry(token, payload, 'iss', user)
    with pytest.raises(KeyError):
        check_token_entry(token, payload, 'uni')


@pytest.mark.parametrize('user', ['testuser', 'testadminuser'])
def test_request_a_token_with_exp_and_uni(client, user):
    headers = get_auth_header(user, 'PASSWORD')
    payload = get_payload(ids=['*'], uni=True)

    response = client.post(TOKEN_EP, data=payload, headers=headers)

    if user == 'testuser':
        assert response.status_code == 412
    else:
        assert response.status_code == 201
        assert 'token' in response.json
        token = response.json['token']
        print(jwt.decode(token, verify=False))
        check_token_entry(token, payload, 'hwid')
        check_token_entry(token, {'ids': ['*']}, 'ids')
        with pytest.raises(AssertionError):
            check_token_entry(token, payload, 'mtds')
        check_token_entry(token, {'mtds': ['GET']}, 'mtds')
        check_token_entry(token, payload, 'exp')
        check_token_entry(token, payload, 'iss', user)
        check_token_entry(token, payload, 'uni')


def test_request_a_token_without_exp_normal_user(client):
    user = 'testuser'
    headers = get_auth_header(user, 'PASSWORD')
    payload = get_payload(exp=-1)
    # user testuser has not the privileges for exp = -1
    response = client.post(TOKEN_EP, data=payload, headers=headers)
    assert response.status_code == 412


def test_request_a_token_with_out_exp_admin_user(client):
    user = 'testadminuser'
    headers = get_auth_header(user, 'PASSWORD')
    payload = get_payload(exp=-1)
    # user testadminuser has the privileges for exp = -1
    response = client.post(TOKEN_EP, data=payload, headers=headers)
    assert response.status_code == 201
    assert 'token' in response.json
    token = response.json['token']
    check_token_entry(token, payload, 'exp')


@pytest.mark.parametrize('user', ['testuser', 'testadminuser'])
def test_request_a_token_with_invalid_mtds(client, user):
    user = 'testuser'
    headers = get_auth_header(user, 'PASSWORD')
    payload = get_payload(mtds=['SET'])
    # invalid http methode
    response = client.post(TOKEN_EP, data=payload, headers=headers)
    assert response.status_code == 412


def test_request_a_token_with_unprivileged_mtds_normal_user(client):
    user = 'testuser'
    headers = get_auth_header(user, 'PASSWORD')
    payload = get_payload(mtds=['PUT'])
    # user has no permission for PUT
    response = client.post(TOKEN_EP, data=payload, headers=headers)
    assert response.status_code == 412


def test_request_a_token_with_all_mtds_admin_user(client):
    user = 'testadminuser'
    headers = get_auth_header(user, 'PASSWORD')
    payload = get_payload(mtds=['DELETE', 'GET', 'PATCH', 'POST', 'PUT'])
    # user has permisson for all methodes
    response = client.post(TOKEN_EP, data=payload, headers=headers)
    assert response.status_code == 201
    assert 'token' in response.json
    token = response.json['token']
    check_token_entry(token, payload, 'mtds')


@pytest.mark.parametrize('ids', ['*', 'Sensor 2', 'ä', '.', '-', ''])
@pytest.mark.parametrize('user', ['testuser', 'testadminuser'])
def test_request_a_token_with_invalid_ids(client, user, ids):
    user = 'testuser'
    headers = get_auth_header(user, 'PASSWORD')
    payload = get_payload(ids=[ids])
    # invalid http methode
    response = client.post(TOKEN_EP, data=payload, headers=headers)
    assert response.status_code == 400

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
