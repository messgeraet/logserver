"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        test_sensor_sensorgetlist_get.py

    FIRST RELEASE
        2017-09-15  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import time

import pytest  # pylint:disable=W0611

from utils import get_token, SENSORGETVALUE_EP
from app.models import DataPoint, User

# pylint: disable=C0103,W0613,C0111

GOOD_SENSOR_VALUES = [
    SENSORGETVALUE_EP.format(sensor=val, timestamp='')
    for val in ['test1', 'test2', 'test3']]


@pytest.fixture(params=GOOD_SENSOR_VALUES, ids=GOOD_SENSOR_VALUES)
def good_url(request):
    return request.param


def test_sensorvalue_no_token(client, good_url):
    sensor = good_url.split('/')[-2]
    timestamp = DataPoint.query.filter_by(sensor=sensor).first().timestamp
    url = good_url + str(timestamp)
    response = client.get(url)
    assert response.status_code == 403


def test_sensorvalue_exp_token(app, client, good_url):
    header = get_token(exp=-3, test=True)
    sensor = good_url.split('/')[-2]
    timestamp = DataPoint.query.filter_by(sensor=sensor).first().timestamp
    url = good_url + str(timestamp)
    response = client.get(url, headers=header)
    assert response.status_code == 403


def test_sensorvalue_no_get_token(app, client, good_url):
    header = get_token(mtds=['POST'])
    sensor = good_url.split('/')[-2]
    timestamp = DataPoint.query.filter_by(sensor=sensor).first().timestamp
    url = good_url + str(timestamp)
    response = client.get(url, headers=header)
    assert response.status_code == 406


def test_sensorvalue_all_forbidden_methodes(app, client, good_url):
    user = User.query.filter_by(username='testadminuser').first()
    header = get_token(mtds=['DELETE', 'PATCH', 'POST', 'PUT'], user=user)
    header.append(('hwid', 'HARDWAREID'))
    url = good_url + str(999999999)
    assert client.delete(url, headers=header).status_code == 405
    assert client.patch(url, headers=header).status_code == 405
    assert client.post(url, headers=header).status_code == 405
    assert client.put(url, headers=header).status_code == 405


def test_sensorvalue(app, client, good_url):
    header = get_token()
    sensor = good_url.split('/')[-2]
    timestamp = DataPoint.query.filter_by(sensor=sensor).first().timestamp
    url = good_url + str(timestamp)
    response = client.get(url, headers=header)
    assert response.status_code == 200
    print(response.json)
    assert response.json['sensor'] == sensor
    assert response.json['timestamp'] == timestamp
    assert 'sensor_value' in response.json['value'].keys()
    prefix = 'http://localhost'
    assert response.json['uri'] == prefix + url


def test_sensorvalue_with_uni_token(app, client, good_url):
    user = User.query.filter_by(username='testadminuser').first()
    header = get_token(user=user, uni=True)
    sensor = good_url.split('/')[-2]
    timestamp = DataPoint.query.filter_by(sensor=sensor).first().timestamp
    url = good_url + str(timestamp)
    response = client.get(url, headers=header)
    assert response.status_code == 200
    print(response.json)
    assert response.json['sensor'] == sensor
    assert response.json['timestamp'] == timestamp
    assert 'sensor_value' in response.json['value'].keys()
    prefix = 'http://localhost'
    assert response.json['uri'] == prefix + url


@pytest.mark.parametrize('sensor_id', ['*', 'Sensor 2', 'ä', '.', '-', ' '])
def test_sensorvalue_with_uni_token_invalid_url(app, client, sensor_id):
    user = User.query.filter_by(username='testadminuser').first()
    header = get_token(user=user, uni=True)
    url = SENSORGETVALUE_EP.format(sensor=sensor_id, timestamp='12345')
    response = client.get(url, headers=header)
    assert response.status_code == 406


def test_sensorvalues_bad_url(app, client):
    header = get_token(ids=['bad_sensor'])
    bad_url = SENSORGETVALUE_EP.format(sensor='bad_sensor', timestamp=123)
    response = client.get(bad_url, headers=header)
    assert response.status_code == 404


def test_sensorvalues_bad_url_big_timestamp(app, client):
    header = get_token(ids=['bad_sensor'])
    timestamp = '9999999999'
    bad_url = SENSORGETVALUE_EP.format(sensor='bad_sensor',
                                       timestamp=timestamp)
    response = client.get(bad_url, headers=header)
    assert response.status_code == 404


def test_sensorvalues_bad_url_timestamp_to_long(app, client):
    header = get_token(ids=['bad_sensor'])
    timestamp = str(int(time.time()))*2
    bad_url = SENSORGETVALUE_EP.format(sensor='bad_sensor',
                                       timestamp=timestamp)
    response = client.get(bad_url, headers=header)
    assert response.status_code == 404


def test_sensorvalues_bad_url_timestamp_no_int(app, client):
    header = get_token(ids=['bad_sensor'])
    timestamp = 'timestamp'
    bad_url = SENSORGETVALUE_EP.format(sensor='bad_sensor',
                                       timestamp=timestamp)
    response = client.get(bad_url, headers=header)
    assert response.status_code == 404


def test_sensorvalues_bad_url_empty_timestamp(app, client):
    header = get_token(ids=['bad_sensor'])
    timestamp = ''
    bad_url = SENSORGETVALUE_EP.format(sensor='bad_sensor',
                                       timestamp=timestamp)
    response = client.get(bad_url, headers=header)
    assert response.status_code == 404
# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
