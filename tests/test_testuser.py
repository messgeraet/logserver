"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        test_token.py

    FIRST RELEASE
        2017-07-27  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import pytest  # pylint:disable=W0611

from app.models import User, Permission
from utils import get_auth_header, TOKEN_EP

# pylint: disable=C0103,W0613,C0111


def test_user(app):
    """check testusers"""
    testuser = User.query.filter_by(username='testuser').first()
    assert testuser is not None
    assert testuser.is_active
    assert not testuser.is_administrator()
    assert testuser.can(Permission.GRANT_GET)
    assert testuser.can(Permission.GRANT_POST)

    adminuser = User.query.filter_by(username='testadminuser').first()
    assert adminuser is not None
    assert adminuser.is_active
    assert adminuser.is_administrator()


def test_new_admin_user(app, client):
    """check ADMIN"""
    user = 'ADMIN'
    headers = get_auth_header(user, 'PASSWORD')
    assert client.put(TOKEN_EP, headers=headers).status_code == 405

    adminuser = User.query.filter_by(username='ADMIN').first()
    assert adminuser is not None
    assert adminuser.is_active
    assert adminuser.is_administrator()

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
