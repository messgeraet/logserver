"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        conftest.py

    FIRST RELEASE
        2017-07-27  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import pytest

from app import create_app, db
from app.models import User, DataPoint


@pytest.fixture
def app():
    """inject app object"""
    app = create_app('testing')
    with app.app_context():
        db.create_all()
        User.create_testusers()
        DataPoint.create_testdatapoints()
    return app

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
