"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        test_sensor.py


    FIRST RELEASE
        2017-08-12  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import pytest  # pylint:disable=W0611

from utils import get_token, SENSOR_EP

from app.models import User

# pylint: disable=C0103,W0613,C0111


def test_sensornames_no_token(client):
    response = client.get(SENSOR_EP)
    assert response.status_code == 403


def test_sensornames_exp_token(app, client):
    header = get_token(exp=-3, test=True)
    response = client.get(SENSOR_EP, headers=header)
    assert response.status_code == 403


def test_sensornames_no_get_token(app, client):
    header = get_token(mtds=['POST'])
    response = client.get(SENSOR_EP, headers=header)
    assert response.status_code == 406


def test_sensornames(app, client):
    header = get_token()
    response = client.get(SENSOR_EP, headers=header)
    assert response.status_code == 200
    assert 'test1' in response.json['sensors']
    assert 'test2' in response.json['sensors']
    assert 'test3' in response.json['sensors']
    assert response.json['paginate']['page'] == 1
    assert response.json['paginate']['per_page'] == 10
    assert response.json['paginate']['pages'] == 1
    assert response.json['paginate']['total'] == 3
    assert response.json['paginate']['links']['next_page'] == ''
    assert response.json['paginate']['links']['prev_page'] == ''
    prefix = 'http://localhost'
    query = '?page=1&per_page=10'
    assert response.json['uri'] == prefix + SENSOR_EP + query


def test_sensornames_resticted_ids_set(app, client):
    header = get_token(ids=['test2', 'test'])
    response = client.get(SENSOR_EP, headers=header)
    assert response.status_code == 200
    assert 'test1' not in response.json['sensors']
    assert 'test2' in response.json['sensors']
    assert 'test3' not in response.json['sensors']
    assert response.json['paginate']['page'] == 1
    assert response.json['paginate']['per_page'] == 10
    assert response.json['paginate']['pages'] == 1
    assert response.json['paginate']['total'] == 1
    assert response.json['paginate']['links']['next_page'] == ''
    assert response.json['paginate']['links']['prev_page'] == ''
    prefix = 'http://localhost'
    query = '?page=1&per_page=10'
    assert response.json['uri'] == prefix + SENSOR_EP + query


def test_sensornames_with_uni_token(app, client):
    user = User.query.filter_by(username='testadminuser').first()
    header = get_token(user=user, uni=True)
    response = client.get(SENSOR_EP, headers=header)
    assert response.status_code == 200
    assert 'test1' in response.json['sensors']
    assert 'test2' in response.json['sensors']
    assert 'test3' in response.json['sensors']
    assert response.json['paginate']['page'] == 1
    assert response.json['paginate']['per_page'] == 10
    assert response.json['paginate']['pages'] == 1
    assert response.json['paginate']['total'] == 3
    assert response.json['paginate']['links']['next_page'] == ''
    assert response.json['paginate']['links']['prev_page'] == ''
    prefix = 'http://localhost'
    query = '?page=1&per_page=10'
    assert response.json['uri'] == prefix + SENSOR_EP + query


def test_sensornames_paginated(app, client):
    header = get_token()
    query = '?page=1&per_page=1'
    response = client.get(SENSOR_EP + query, headers=header)
    assert response.status_code == 200
    assert 'test1' in response.json['sensors']
    assert 'test2' not in response.json['sensors']
    assert response.json['paginate']['page'] == 1
    assert response.json['paginate']['per_page'] == 1
    assert response.json['paginate']['pages'] == 3
    assert response.json['paginate']['total'] == 3
    prefix = 'http://localhost'
    next_query = '?page=2&per_page=1'
    assert response.json['paginate']['links']['next_page'] == \
        prefix + SENSOR_EP + next_query
    assert response.json['paginate']['links']['prev_page'] == ''
    assert response.json['uri'] == prefix + SENSOR_EP + query

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
