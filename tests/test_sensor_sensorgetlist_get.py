"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        test_sensor_sensorgetlist_get.py

    FIRST RELEASE
        2017-09-15  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import pytest  # pylint:disable=W0611

from utils import get_token, SENSORGETLIST_EP

from app.models import User

# pylint: disable=C0103,W0613,C0111

GOOD_SENSOR_IDS = [
    SENSORGETLIST_EP.format(sensor=val)
    for val in ['test1', 'test2', 'test3']]


@pytest.fixture(params=GOOD_SENSOR_IDS, ids=GOOD_SENSOR_IDS)
def good_url(request):
    return request.param


def test_sensorvalues_no_token(client, good_url):
    response = client.get(good_url)
    assert response.status_code == 403


def test_sensorvalues_exp_token(app, client, good_url):
    header = get_token(exp=-3, test=True)
    response = client.get(good_url, headers=header)
    assert response.status_code == 403


def test_sensorvalues_no_get_token(app, client, good_url):
    header = get_token(mtds=['POST'])
    response = client.get(good_url, headers=header)
    assert response.status_code == 406


def test_sensorvalues_all_forbidden_methodes(app, client, good_url):
    user = User.query.filter_by(username='testadminuser').first()
    header = get_token(mtds=['DELETE', 'PATCH', 'POST', 'PUT'], user=user)
    header.append(('hwid', 'HARDWAREID'))
    assert client.delete(good_url, headers=header).status_code == 405
    assert client.patch(good_url, headers=header).status_code == 405
    assert client.post(good_url, headers=header).status_code == 405
    assert client.put(good_url, headers=header).status_code == 405


def test_sensorvalues(app, client, good_url):
    header = get_token()
    response = client.get(good_url, headers=header)
    assert response.status_code == 200
    assert good_url.split('/')[-1] == response.json['values'][0]['sensor']
    assert response.json['paginate']['page'] == 1
    assert response.json['paginate']['per_page'] == 10
    assert response.json['paginate']['pages'] == 4
    assert response.json['paginate']['total'] == 33
    prefix = 'http://localhost'
    next_query = '?page=2&per_page=10'
    next_url = prefix + good_url + next_query
    assert response.json['paginate']['links']['next_page'] == next_url
    assert response.json['paginate']['links']['prev_page'] == ''
    query = '?page=1&per_page=10'
    assert response.json['uri'] == prefix + good_url + query


def test_sensorvalues_with_uni_token(app, client, good_url):
    user = User.query.filter_by(username='testadminuser').first()
    header = get_token(user=user, uni=True)
    response = client.get(good_url, headers=header)
    assert response.status_code == 200
    assert good_url.split('/')[-1] == response.json['values'][0]['sensor']
    assert response.json['paginate']['page'] == 1
    assert response.json['paginate']['per_page'] == 10
    assert response.json['paginate']['pages'] == 4
    assert response.json['paginate']['total'] == 33
    prefix = 'http://localhost'
    next_query = '?page=2&per_page=10'
    next_url = prefix + good_url + next_query
    assert response.json['paginate']['links']['next_page'] == next_url
    assert response.json['paginate']['links']['prev_page'] == ''
    query = '?page=1&per_page=10'
    assert response.json['uri'] == prefix + good_url + query


@pytest.mark.parametrize('sensor_id', ['*', 'Sensor 2', 'ä', '.', '-', ' '])
def test_sensorvalues_with_uni_token_invalid_url(app, client, sensor_id):
    user = User.query.filter_by(username='testadminuser').first()
    header = get_token(user=user, uni=True)
    bad_url = SENSORGETLIST_EP.format(sensor=sensor_id)
    response = client.get(bad_url, headers=header)
    assert response.status_code == 406


def test_sensorvalues_paginated(app, client, good_url):
    header = get_token()
    query = '?page=2&per_page=11'
    response = client.get(good_url + query, headers=header)
    assert response.status_code == 200
    assert good_url.split('/')[-1] == response.json['values'][0]['sensor']
    assert response.json['paginate']['page'] == 2
    assert response.json['paginate']['per_page'] == 11
    assert response.json['paginate']['pages'] == 3
    assert response.json['paginate']['total'] == 33
    prefix = 'http://localhost'
    next_query = '?page=3&per_page=11'
    next_url = prefix + good_url + next_query
    prev_query = '?page=1&per_page=11'
    prev_url = prefix + good_url + prev_query
    assert response.json['paginate']['links']['next_page'] == next_url
    assert response.json['paginate']['links']['prev_page'] == prev_url
    assert response.json['uri'] == prefix + good_url + query


def test_sensorvalues_bad_url_page_eq_1(app, client):
    header = get_token(ids=['bad_sensor'])
    bad_url = SENSORGETLIST_EP.format(sensor='bad_sensor')
    response = client.get(bad_url, headers=header)
    assert response.status_code == 200
    assert not response.json['values']
    assert response.json['paginate']['page'] == 1
    assert response.json['paginate']['per_page'] == 10
    assert response.json['paginate']['pages'] == 0
    assert response.json['paginate']['total'] == 0
    assert response.json['paginate']['links']['next_page'] == ''
    assert response.json['paginate']['links']['prev_page'] == ''
    prefix = 'http://localhost'
    query = '?page=1&per_page=10'
    assert response.json['uri'] == prefix + bad_url + query


def test_sensorvalues_bad_url_page_gt_1(app, client):
    header = get_token(ids=['bad_sensor'])
    bad_url = SENSORGETLIST_EP.format(sensor='bad_sensor')
    query = '?page=2'
    response = client.get(bad_url + query, headers=header)
    assert response.status_code == 404

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
