"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        test_sensor.py


    FIRST RELEASE
        2017-08-12  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import json

import pytest  # pylint:disable=W0611

from app.models import DataPoint
from utils import get_token, SENSOR_EP

# pylint: disable=C0103,W0613,C0111


def test_post_no_token_no_payload(client):
    response = client.post(SENSOR_EP)
    assert response.status_code == 403


def test_post_no_token_with_payload(client):
    sensor = 'posttest0'
    data = {'hwid': 'HWID', 'sensor': sensor, 'value': '"hallo"'}
    response = client.post(
        SENSOR_EP,
        data=json.dumps(data),
        content_type='application/json')
    assert response.status_code == 403


def test_simple_post_with_token(app, client):
    sensor = 'posttest1'
    value = 'hallo'
    data = {'hwid': 'HWID', 'sensor': sensor, 'value': value}
    header = get_token(ids=[sensor], test=True)
    response = client.post(
        SENSOR_EP,
        data=json.dumps(data),
        content_type='application/json',
        headers=header)
    assert response.status_code == 201

    # Check entry in database
    datapoints = DataPoint.query.filter_by(sensor=sensor).all()
    assert len(datapoints) == 1
    value_get = datapoints[0].to_dict(value_decode=True)['value']
    assert value_get == value


def test_post_with_token_but_without_value(app, client):
    sensor = 'posttest1'
    data = {'hwid': 'HWID', 'sensor': sensor}
    header = get_token(ids=[sensor], test=True)
    response = client.post(
        SENSOR_EP,
        data=json.dumps(data),
        content_type='application/json',
        headers=header)
    assert response.status_code == 400


def test_post_with_token(app, client):
    sensor = 'posttest2'
    header = get_token(ids=[sensor], test=True)
    value = {'key': 'value', 'listkey': [1, 2, 3], 'dictkey': {'x': 7.6}}
    data = {'hwid': 'HWID', 'sensor': sensor, 'value': value}
    response = client.post(
        SENSOR_EP,
        data=json.dumps(data),
        content_type='application/json',
        headers=header)
    assert response.status_code == 201

    # Check entry in database
    datapoints = DataPoint.query.filter_by(sensor=sensor).all()
    assert len(datapoints) == 1
    value_get = datapoints[0].to_dict(value_decode=True)['value']
    for key in value:
        assert value[key] == value_get[key]


def test_post_with_exp_token(app, client):
    sensor = 'posttest3'
    header = get_token(exp=-3, ids=[sensor], test=True)
    value = {'key': 'value', 'listkey': [1, 2, 3], 'dictkey': {'x': 7.6}}
    data = {'hwid': 'HWID', 'sensor': sensor, 'value': value}
    response = client.post(
        SENSOR_EP,
        data=json.dumps(data),
        content_type='application/json',
        headers=header)
    assert response.status_code == 403


def test_post_with_token_wrong_hwid(app, client):
    sensor = 'posttest4'
    header = get_token(hwid='XXX', ids=[sensor], test=True)
    value = {'key': 'value', 'listkey': [1, 2, 3], 'dictkey': {'x': 7.6}}
    data = {'hwid': 'HWID', 'sensor': sensor, 'value': value}
    response = client.post(
        SENSOR_EP,
        data=json.dumps(data),
        content_type='application/json',
        headers=header)
    assert response.status_code == 406


def test_post_with_token_wrong_ids(app, client):
    sensor = 'posttest5'
    header = get_token(hwid='XXX', ids=[sensor], test=True)
    value = {'key': 'value', 'listkey': [1, 2, 3], 'dictkey': {'x': 7.6}}
    data = {'hwid': 'HWID', 'sensor': sensor, 'value': value}
    response = client.post(
        SENSOR_EP,
        data=json.dumps(data),
        content_type='application/json',
        headers=header)
    assert response.status_code == 406


def test_post_with_token_wrong_mtds(app, client):
    sensor = 'posttest6'
    header = get_token(mtds=['GET'], ids=[sensor], test=True)
    value = {'key': 'value', 'listkey': [1, 2, 3], 'dictkey': {'x': 7.6}}
    data = {'hwid': 'HWID', 'sensor': sensor, 'value': value}
    response = client.post(
        SENSOR_EP,
        data=json.dumps(data),
        content_type='application/json',
        headers=header)
    assert response.status_code == 406


def test_multiple_fast_simple_post_with_token(app, client):
    sensor = 'posttest7'
    value = 'hallo'
    data = {'hwid': 'HWID', 'sensor': sensor, 'value': value}
    header = get_token(ids=[sensor], test=True)
    result = []
    for _ in range(200):
        response = client.post(
            SENSOR_EP,
            data=json.dumps(data),
            content_type='application/json',
            headers=header)
        result.append(response.status_code)

    assert 201 in result and 409 in result

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
