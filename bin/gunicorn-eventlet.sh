#!/bin/bash
# vim: ft=sh:tw=80:ts=4:sta:sw=4:si:ci
#************************************************************** SHELL SCRIPT ***
#   NAME
#       gunicorn-eventlet.sh
#
#
#   FIRST RELEASE
#       2017-02-27  Sebastian Stigler		sebastian.stigler@hs-aalen.de
#
#   COPYRIGHT (C) 2017
#*******************************************************************************
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -o allexport
source ${DIR}/../.env
set +o allexport

if [ $# -gt 1 ];
then
    PORT=$1
else
    PORT=5000
fi

echo "delay 5 seconds"
sleep 5

exec ${DIR}/../venv/bin/gunicorn -b 127.0.0.1:${PORT} -k eventlet -w 1 app.wsgi
