#!/bin/bash
# vim: ft=sh:tw=80:ts=4:sta:sw=4:si:ci
#************************************************************** SHELL SCRIPT ***
#   NAME
#       gunicorn.sh
#
#
#   FIRST RELEASE
#       2017-02-27  Sebastian Stigler		sebastian.stigler@hs-aalen.de
#
#   COPYRIGHT (C) 2017
#*******************************************************************************
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

set -o allexport
source ${DIR}/../.env
set +o allexport


exec $@
