#!/bin/bash
# vim: ft=sh:tw=80:ts=4:sta:sw=4:si:ci
#************************************************************** SHELL SCRIPT ***
#   NAME
#       install.sh
#
#   FIRST RELEASE
#       2017-02-28  Sebastian Stigler		sebastian.stigler@hs-aalen.de
#
#   COPYRIGHT (C) 2017
#*******************************************************************************
#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )"

#kill existing supervisor process
pkill -SIGTERM supervisord

#create supervisor upstart script
(cat $DIR/logserver.service ; echo "ExecStart=/usr/bin/supervisord --nodaemon --configuration $BASEDIR/config/supervisord.conf") > /etc/systemd/system/logserver.service
#reload services
systemctl daemon-reload

#enable services on boot
systemctl enable logserver.service
# create supervisorctl-logserver script
echo -e '#!/bin/bash\n/usr/bin/supervisorctl -s unix:///tmp/supervisor.sock ${@:1}' > /usr/local/bin/logserverctl
chmod 755 /usr/local/bin/logserverctl
#*********************************************************************** END ***

