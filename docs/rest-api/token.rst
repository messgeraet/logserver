.. role:: hll

Endpoint: token
---------------

This endpoint creates a new JWT token.


Method
^^^^^^

.. table::

    ====== ============== ============== ====================
    Method Endpoint       Authentication Description
    ====== ============== ============== ====================
    POST   /api/v1/token  Basic          Request a new token
    ====== ============== ============== ====================

Parameter
^^^^^^^^^

.. table::

   +------+----------+-------------------------------------------------------+
   | NAME | Type     | Description                                           |
   +======+==========+=======================================================+
   | hwid | string   | **Required** Hardware ID of the device from which the |
   |      |          | token will be used. This ID will only be used for     |
   |      |          | posting new data for the database.                    |
   +------+----------+-------------------------------------------------------+
   | ids  | array of | **Required** A list of sensor_ids for which the token |
   |      | strings  | is valid.                                             |
   +------+----------+-------------------------------------------------------+
   | mtds | array of | **Required** A list of HTTP methodes for which the    |
   |      | strings  | token is valid. The methods are: GET, DELETE, PATCH,  |
   |      |          | POST and PUT.                                         |
   |      |          | *Currently only GET and POST are supported.*          |
   +------+----------+-------------------------------------------------------+
   | exp  | integer  | **Required** The number of hours the token will be    |
   |      |          | valid when issued. The value `-1` indicates, that the |
   |      |          | token won't expire.                                   |
   +------+----------+-------------------------------------------------------+
   | uni  | boolean  | **Optional** If set to `true` a token is requested    |
   |      |          | which will allow GET request to arbitrary sensor_ids. |
   |      |          | Hence a *universal token*.                            |
   |      |          | If the parameter is ommited `false` will be assumed.  |
   +------+----------+-------------------------------------------------------+

.. important::

   The requesting user needs special privileges to requests the following
   settings:
 
   - Use of HTTP methodes other than GET.
   - To set `exp` to -1.
   - To set `uni` to true.

.. note::

   If your request contains ``"uni": true`` and you have the necessary
   privileges then the decoded token you will get, will have the `ids` and
   `mtds` parameters overwritten with the following settings:

   - ``"ids": ["*"]``
   - ``"mtds": ["GET"]``


Response
^^^^^^^^

Assume we POST the following payload to this endpoint at *26/Jul/2017 11:41:12*
with the user *sebastian*:

.. code-block:: json

   {
     "hwid": "67849ef05f2cf44dd0499a6f20c139e4",
     "ids": ["ID_des_Sensors_in_der_Datenbank"],
     "mtds": ["POST"],
     "exp": 720
   }


.. admonition:: On success

   :hll:`Status: 201 Created`

   .. code-block:: json

      {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJzZWJhc3RpYW4iLCJleHAiOjE1MDM2NTQwNzIsImh3aWQiOiI2Nzg0OWVmMDVmMmNmNDRkZDA0OTlhNmYyMGMxMzllNCIsIm10ZHMiOlsiUE9TVCJdLCJpZHMiOlsiSURfZGVzX1NlbnNvcnNfaW5fZGVyX0RhdGVuYmFuayJdfQ.50d0kZDHkRWv3dUsWsZ1LHBzd448hJPN7gEwtllRroLov5uFqlA2Lj_XCjNGeUF35rB5MHT9rNNA-qdIXpgsrg"
      }

The decoded claims of the above token are:

.. code-block:: json

   {
     "hwid": "67849ef05f2cf44dd0499a6f20c139e4",
     "ids": ["ID_des_Sensors_in_der_Datenbank"],
     "mtds": ["POST"],
     "exp": 1503654072,
     "iss": "sebastian"
   }

.. admonition:: A invalid sensor_id was requested

   :hll:`Status: 400 Bad Request`

   .. code-block:: json

      {
        "message": "The browser (or proxy) sent a request that this server could not understand.",
        "status": 400
      }

.. admonition:: The credentials are invalid

   :hll:`Status: 403 Forbidden`

   .. code-block:: json

      {
        "message": "You don't have the permission to access the requested resource. It is either read-protected or not readable by the server.",
        "status": 403
      }

.. admonition:: The login user has not sufficient privileges

   :hll:`Status: 412 Precondition Failed`

   .. code-block:: json

      {
          "message": "The user has not enough privileges for this request.",
          "status": 412
      }

