Common
------

URL Prefix
^^^^^^^^^^

All endpoints are prefixed with :samp:`/api/v{VERSION}`.
The current version is `1`.

Payload
^^^^^^^

All payloads for requests and responses are in json format.
