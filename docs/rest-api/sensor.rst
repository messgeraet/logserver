.. role:: hll

.. role:: nwr

Endpoint: sensor
----------------

This endpoint stores and retrieves sensor measurements to/from the database.

Methods
^^^^^^^

.. table::

    ====== ============================================== ============== ==========================================
    Method Endpoint                                       Authentication Description
    ====== ============================================== ============== ==========================================
    POST   :nwr:`/api/v1/sensor`                          Token [1]_     :ref:`Store a measurement <store_measurement>`
    GET    :nwr:`/api/v1/sensor`                          Token [1]_     :ref:`Get all sensor ids <get_all_sensor_ids>` [2]_ [3]_
    GET    :nwr:`/api/v1/sensor/{sensor_id}`              Token [1]_     :ref:`Get all measurements for {sensor_id} <get_all_measurements_for_sensor_id>` [3]_
    GET    :nwr:`/api/v1/sensor/{sensor_id}/{timestamp}`  Token [1]_     :ref:`Get measurement for {sensor_id} at {timestamp} <get_measurement_for_sensor_id_at_timestamp>`
    ====== ============================================== ============== ==========================================

.. [1] The token must have the used *HTTP* method in the ``mtds`` field.

.. [2] You get only the ``sensor_id`` which are in the tokens ``ids`` entry.

.. [3] This ``GET`` request is paginate.

.. _use_token:

Usage of the token Authentication
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:doc:`token` describes the creation of a token. To use it, you have to add the
following HTTP header to your request:  ``Authorization: Bearer <token>``.

.. important::
   Beware that an *univeral token* (``"uni": true``) is only for the GET
   requests applicable!

   Further more an *universal token* should only be used between backend
   applications and not for frontend applications (Apps, websites ...).

.. note::
   Example HTTP request to get all server ids. The highlighted line has to be
   added. In the ``example`` directory are examples for ``python`` and
   ``javascript``.

   .. code-block:: http
      :emphasize-lines: 3

      GET /api/v1/sensor HTTP/1.1
      HOST: 127.0.0.1:5000
      Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJzZWJhc3RpYW4iLCJleHAiOjE1MDM2NTQwNzIsImh3aWQiOiI2Nzg0OWVmMDVmMmNmNDRkZDA0OTlhNmYyMGMxMzllNCIsIm10ZHMiOlsiUE9TVCJdLCJpZHMiOlsiSURfZGVzX1NlbnNvcnNfaW5fZGVyX0RhdGVuYmFuayJdfQ.50d0kZDHkRWv3dUsWsZ1LHBzd448hJPN7gEwtllRroLov5uFqlA2Lj_XCjNGeUF35rB5MHT9rNNA-qdIXpgsrg


.. _store_measurement:

Store a mesurement
^^^^^^^^^^^^^^^^^^

Parameter
"""""""""
.. table::

   +--------+----------+-------------------------------------------------------+
   | NAME   | Type     | Description                                           |
   +========+==========+=======================================================+
   | hwid   | string   | **Required** Hardware ID of the device from which the |
   |        |          | measurement is sent.                                  |
   +--------+----------+-------------------------------------------------------+
   | sensor | string   | **Required** The sensor_ids for this measurement.     |
   +--------+----------+-------------------------------------------------------+
   | value  | object   | **Required** The measured values in json format.      |
   +--------+----------+-------------------------------------------------------+

.. note::
   The parameters must be send with ``Content-Type: application/json``

   .. code-block:: json

      {
        "hwid": "67849ef05f2cf44dd0499a6f20c139e4",
        "sensor": "ID_des_Sensors_in_der_Datenbank",
        "value": {
          "constant": "Hallo Welt"
        }
      }

.. important::
   The ``hwid`` and the ``sensor`` must match the payload of the token!

Response
""""""""

.. admonition:: On success

   :hll:`Status: 201 CREATED`

   .. code-block:: json

      {
        "message": "value stored"
      }

.. admonition:: Incomplete parameters

   I.e. no ``value`` entry in the payload

   :hll:`Status: 400 Bad Request`

   .. code-block:: json

      {
        "message": "The browser (or proxy) sent a request that this server could not understand.",
        "status": 400
      }

.. admonition:: The token is invalid

   I.e. not a jwt-token, not token at all or the token has expired.

   :hll:`Status: 403 Forbidden`

   .. code-block:: json

      {
        "message": "You don't have the permission to access the requested resource. It is either read-protected or not readable by the server.",
        "status": 403
      }

.. admonition:: Token is not valid for this payload

   I.e. the ``hwid``, the *HTTP* method or the ``server_id`` doesn't fit the token payload.

   :hll:`Status: 406  Not Acceptable`

   .. code-block:: json

      {
        "message": "Token is not valid for the payload of this request.",
        "status": 406
      }

.. admonition:: Unique constraint violation

   It is not allowed to post two values for the same ``sensor_id`` at the same seconde.

   :hll:`Status: 409  Conflict`

   .. code-block:: json

      {
        "message": "Timestamp conflict. Only one value per seconde and sensor.",
        "status": 409
      }

.. _get_all_sensor_ids:

Get all sensor ids
^^^^^^^^^^^^^^^^^^

Paramters
"""""""""

NONE

Response
""""""""

.. admonition:: On success

   All the sensors, that you are allowed to see (restricted through the token),
   will be returned at ``sensors`` as a list.

   The usage of an *universal token* returns all the ``sensors`` in the
   database as a list.

   The entries at ``paginate`` will explained :ref:`later<paginate>`.
   The ``uri`` entry shows the GET-request, to get this result. The options
   after the ``?`` sign are optional and will be discussed in the
   :ref:`Pagination section<paginate>`.

   :hll:`Status: 200 OK`

   .. code-block:: json

      {
        "sensors": [
          "ID_des_Sensors_in_der_Datenbank"
        ],
        "paginate": {
          "page": 1,
          "per_page": 10,
          "pages": 1,
          "total": 1,
          "links": {
              "next_page": "",
              "prev_page": ""
          }
        },
        "uri": "http://127.0.0.1:5000/api/v1/sensor?page=1&per_page=10"
      }

.. note::

   The responses on error (403 and 406) are the same as in the POST request.
   Only now the ``hwid`` is not checkt aginst the token.


.. _get_all_measurements_for_sensor_id:

Get all measurements for {sensor_id}
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Paramters
"""""""""

NONE

Response
""""""""

.. admonition:: On success

   The all the values for {sensor_id}, will be returned at as a list.
   The entries at ``paginate`` will explained :ref:`later<paginate>`.
   The ``uri`` entry shows the GET-request, to get this result. The options
   after the ``?`` sign are optional and will be discussed in the
   :ref:`Pagination section<paginate>`.

   :hll:`Status: 200 OK`

   .. code-block:: json

      {
        "values": [
          {
            "sensor": "ID_des_Sensors_in_der_Datenbank",
            "timestamp": 1504610047,
            "timestamp_rcf822": "Tue, 05 Sep 17 13:14:07 ",
            "uri": "http://127.0.0.1:5000/api/v1/sensor/ID_des_Sensors_in_der_Datenbank/1504610047",
            "value": {
              "constant": "Hallo Welt"
            }
          }
        ],
        "paginate": {
          "page": 1,
          "per_page": 10,
          "pages": 1,
          "total": 1,
          "links": {
            "next_page": "",
            "prev_page": ""
          }
        },
        "uri": "http://127.0.0.1:5000/api/v1/sensor/ID_des_Sensors_in_der_Datenbank?page=1&per_page=10",
      }

.. note::

   The responses on error (403 and 406) are the same as in the POST request.
   Only now the ``hwid`` is not checkt aginst the token.

   If you use an *universal token* then an invalid ``sensor_id`` (only letters,
   digits and underscore are allowed) results in an 406 error.


.. _get_measurement_for_sensor_id_at_timestamp:

Get measurement for {sensor_id} at {timestamp}
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Paramters
"""""""""

NONE

Response
""""""""

.. admonition:: On success

   The value for {sensor_id} at {timstamp} , will be returned at as a object.

   :hll:`Status: 200 OK`

   .. code-block:: json

      {
        "sensor": "ID_des_Sensors_in_der_Datenbank",
        "timestamp": 1504610047,
        "timestamp_rcf822": "Tue, 05 Sep 17 13:14:07 ",
        "uri": "http://127.0.0.1:5000/api/v1/sensor/ID_des_Sensors_in_der_Datenbank/1504610047",
        "value": {
          "constant": "Hallo Welt"
        }
      }

.. note::

   The responses on error (403 and 406) are the same as in the POST request.
   Only now the ``hwid`` is not checkt aginst the token.

   If you use an *universal token* then an invalid ``sensor_id`` (only letters,
   digits and underscore are allowed) results in an 406 error.

.. _paginate:

Pagination
^^^^^^^^^^

The result of :ref:`Get all measurements for {sensor_id} <get_all_measurements_for_sensor_id>`
and
:ref:`Get measurement for {sensor_id} at {timestamp} <get_measurement_for_sensor_id_at_timestamp>`
are paginated. I.e. you don't get all results to your query but just a
fragment of them with the additional informations to get the next and the
previous fragment. Furthermore there are informations on how many results there
are altogether, on what page you are and how many pages there are.

The default setting is, that you get at most 10 results per request. The
parameter ``per_page``, which you can append to the http request, denotes the
number of results per page. It can be set to a value from ``1`` to ``100``.

To choose an another page then the first you have to append the http request
with the ``page`` parameter.  If the page parameter is out of the range from
``1`` to the total number of pages, you get an 404 error.

.. note::
   The following example shows the result of the query for the second page of
   the ``ID_des_Sensors_in_der_Datenbank`` sensor with 10 results per page and
   20 results total:

   .. code-block:: json

      {
        "values": [
          "..."
        ],
        "paginate": {
          "page": 2,
          "per_page": 10,
          "pages": 2,
          "total": 20,
          "links": {
            "next_page": "",
            "prev_page": "http://127.0.0.1:5000/api/v1/sensor/ID_des_Sensors_in_der_Datenbank?page=1&per_page=10",
          }
        },
        "uri": "http://127.0.0.1:5000/api/v1/sensor/ID_des_Sensors_in_der_Datenbank?page=2&per_page=10",
      }

   If the ``next_page`` or the ``prev_page`` entry are empty strings, then
   there is no next or previous page. But if the entry is not empty then you
   can use this link to to get the next or previous page.

