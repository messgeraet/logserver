Installation
============

This guide is a successive installation manual. **Each step may depend on the
previous one!**

Requirements
------------

This manual assumes, that you install the app on an Raspberry Pi with `Raspbian
Jessie` or on a server with `Ubuntu 14.04` or `16.04`. Furthermore you need an
active internet connection.

Install system packages
-----------------------

You need to install the packages :program:`nginx`, :program:`git`,
:program:`python-virtualenv`, :program:`python3-pip` and :program:`supervisor`.

::

   sudo apt-get install nginx git python-virtualenv python3-pip supervisor -y

Optional you have to install :program:`postgresql`,
:program:`postgresql-client` and :program:`libpq-dev` if you don't want to use
the :program:`sqlite3` database. ::

   sudo apt-get install postgresql postgresql-client libpq-dev -y

.. _postgresql:

Configuration for :program:`postgresql`
---------------------------------------
.. hint::

   If you don't use :program:`postgresql` you can skip this section and
   continue with the :ref:`creation of the system user and group
   <system_user_group>`.

1. Change the password for the **database user** `postgres`.

   ::

       sudo -u postgres psql
       # in the postgres prompt you have to type:
       \password postgres
       # type your new password and quit with:
       \q

2. Create a new **database user** `logserver` for the app.

   ::

       sudo -u postgres createuser -P logserver

3. Create a new **database** for the app.

   ::

       sudo -u postgres createdb -O logserver logserverdb

.. tip::

  Store the passwords in a password safe!


.. _system_user_group:

Create system user and group
----------------------------

Create the system-group `logserver` and the system-user `logserver`. ::

   sudo groupadd --system logserver
   sudo useradd --system -m -g logserver logserver

.. important::

   If you don't authenticate login users with :program:`ldap` or
   :program:`winbind`, you have to set a password for this user in order to to
   create tokens! ::

      sudo passwd logserver

Clone the repository
--------------------

Change to the `logserver` user and clone the repository for this app. ::

    sudo -iu logserver
    git clone https://gitlab.com/messgeraet/logserver.git

.. important::
   You need an account on https://gitlab.com and have to type the credentials,
   when the :command:`git clone` command requests them.

Create the virtualenv and install the python requirements
---------------------------------------------------------

You have to do the following tasks as user `logserver`:

- create a :program:`virtualenv` with the name :file:`venv`
- activate the :program:`virtualenv`
- install the :program:`python` requirements for :program:`logserver`

::

   cd ~logserver/logserver
   virtualenv -p python3 venv
   source venv/bin/activate
   pip install -r requirements.txt

If you want to use :program:`postgresql` you have to install the following
requirements, too. ::

   pip install -r postgresql-requirements.txt

To automatically activate the necessary environment variables add the following
lines to the :file:`~logserver/.profile`. If there is no such file create it.

::

    source /home/logserver/logserver/venv/bin/activate
    export FLASK_APP=/home/logserver/logserver/flasky.py

Now you can manually execute the instructions in this file with
:command:`source ~logserver/.profile`. Next time you login as user `logserver`
this happens automatically.


Configure the app
-----------------

The app will be configured via environment variables which will be stored in
:file:`~logserver/logserver/.env-file`. To create this file, run as user
`logserver` the following command::

   flask create_env

The file looks like this:

.. literalinclude:: env-file
   :language: sh

**Keep the first three lines of this file as they are!**

.. envvar:: DATABASE_URL

   The current value indicates that you want to use the build in
   :program:`sqlite3` database. If you want to use :program:`postgresql` you
   have to use the following value: ::

     DATABASE_URL=postgresql://logserver:<password>@localhost/logserverdb

   Where ``<password>`` must be repaced with the password for the database user
   `logserver` chosen at :ref:`Configuration for postgresql <postgresql>` step
   2.

.. envvar:: PAM_MODUL

   The value describes which *pam module* (in :file:`/etc/pam.d`) of the os
   should be used to check the password of the user which requests a new token.
   If you use :file:`common-auth` you can verify the local user `logserver` and
   all :program:`ldap` or :program:`winbind` users.  In
   :file:`~logserver/logserver/config` is the *pam modul*
   :file:`logserver-auth` which can be used to restrict the users to members of
   a specific group. You need to copy this file to :file:`/etc/pam.d/` (as
   `root`) and change the :const:`groupname` in the script from `logserver` to
   the group you want to use.

.. envvar:: URL_PREFIX


   With this parameter you can change the prefix of the endpoints to an other
   name. (You must change the url at :attr:`location` in the
   :program:`nginx` settings but **not** the :attr:`proxy_pass` entry!)


You can (as user `logserver`) create your database with the following command
(for :program:`postgresql` users: Configure :program:`postgresql` **before**
this command!). ::

     ~logserver/logserver/bin/runenv.sh flask db upgrade

Configure :program:`nginx` as reverse proxy
-------------------------------------------

From now on you work with your standard user again (simply type :command:`exit`
to go back from user `logserver` to the previous user).

:file:`~logserver/logserver/config/nginx-logserver.conf` is a template config
file for :program:`nginx`. If you don't want to use :program:`nginx` for
anything else, you can simply use this file as the default config. ::

   sudo cp ~logserver/logserver/config/nginx-logserver.conf /etc/nginx/sites-available/default

If you want to integrate it in an already existing config you have to add the
following block before the :attr:`server` section::

   upstream logserver_nodes {
       server 127.0.0.1:5000;
   }

and within the :attr:`server` block::

   location /app {
       proxy_pass http://logserver_nodes;
       proxy_redirect off;

       proxy_set_header Host $host;
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
   }

The url at :attr:`location` should match the :envvar:`URL_PREFIX`.

After your changes to the :program:`nginx` config-file you have to tell
:program:`nginx` to load it. ::

   sudo service nginx reload

Install the start script
------------------------

In :file:`~logserver/logserver/bin` are two subfolders dedicated to the init
systems :file:`systemd` and :file:`upstart`.

Choose the folder for the init system that your os uses. **In this folder** run
the following command::

   sudo ./install.sh

Now you can start your application with::

  sudo -u logserver logserverctl start logserver

The application will automatically start on system start.


