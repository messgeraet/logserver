.. Messgeraet-logserver documentation master file, created by
   sphinx-quickstart on Wed Jul 26 09:55:18 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Messgeraet-logserver's documentation!
================================================
This is the documentation of **logserver**.

Contents
========

.. toctree::
   :maxdepth: 2

   manual/index

   rest-api/index

   License <license>
   Authors <authors>
   Changelog <changes>
   Module Reference <api/modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
