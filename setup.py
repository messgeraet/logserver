"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        setup.py

    FIRST RELEASE
        2017-07-27  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
from setuptools import setup, find_packages

setup(
    name='app',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'flask',
    ],
    setup_requires=[
        'pytest-runner',
    ],
    tests_require=[
        'pytest',
    ],
)


# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
