# pylint: skip-file
import configparser
import requests

from pprint import pprint
config = configparser.ConfigParser()
config.read('token.ini')

token = config.get('Token', 'token')


headers = {'Authorization': "Bearer " + token}

url = "http://127.0.1:5000/api/v1/sensor"

data = {'value': {'messwert': 42}, 'sensor': 'die_antwort', 'hwid': 'xxx'}
print('\nPOST NEW DATA:')
pprint(data)
r = requests.post(url, json=data, headers=headers)
print(r.status_code)
pprint(r.json())

print('\nREQUEST: ' + url + '?per_page=2&page=1')
r = requests.get(url + '?per_page=2&page=1', headers=headers)
print(r.status_code)
pprint(r.json())

print('\nREQUEST: ' + url + '/die_antwort')
r = requests.get(url + '/die_antwort', headers=headers)
print(r.status_code)
pprint(r.json())

print(
    '\nREQUEST: ' + url + '/ID_des_Sensors_in_der_Datenbank?per_page=3&page=1')
r = requests.get(
    url + '/ID_des_Sensors_in_der_Datenbank?per_page=3&page=1',
    headers=headers)
print(r.status_code)
pprint(r.json())

urlx = r.json()['values'][0]['uri']
print('\nREQUEST: ' + urlx)
r = requests.get(urlx, headers=headers)
pprint(r.status_code)
pprint(r.json())

print('\nBad REQUEST:')
r = requests.get(
    url + '/ID_des_Sensors_in_der_Datenbank/150117014x?per_page=2&page=1',
    headers=headers)
pprint(r.status_code)
pprint(r.json())
