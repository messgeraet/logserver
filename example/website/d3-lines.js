window.onload = (function() {
    var bwidth = document.getElementsByTagName('body')[0].offsetWidth - 60;
    var margin = {top: 30, right: 55, bottom: 110, left: 60},
    width = bwidth - margin.left - margin.right,
    height = 540 - margin.top - margin.bottom;

    // Parse the date / time
    var parseDate = d3.time.format("%a, %d %b %y %H:%M:%S ").parse;

    // specify the scales for each set of data
    var x = d3.time.scale().range([0, width]);
    var y0 = d3.scale.linear().range([height, 0]);
    var y1 = d3.scale.linear().range([height, 0]);

    // axis formatting
    var xAxis = d3.svg.axis().scale(x)
        .orient("bottom")
        .innerTickSize(-height)
        .outerTickSize(2)
        .tickPadding(10)
        .ticks(14)
        .tickFormat(d3.time.format("%d. %b %H:%M:%S"));
    var yAxisLeft = d3.svg.axis().scale(y0)
        .orient("left")
        .innerTickSize(-width)
        .outerTickSize(0)
        .tickPadding(10)
        .ticks(5)
        .tickFormat(d3.format(".1f"));
    var yAxisRight = d3.svg.axis().scale(y1)
        .orient("right")
        .innerTickSize(-width)
        .outerTickSize(0)
        .tickPadding(10)
        .ticks(5)
        .tickFormat(d3.format(".1f"));
    // line functions
    var temperatureLine = d3.svg.line()
        // .interpolate("basis")
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y0(d.temperature); });
    var pressureLine = d3.svg.line()
        // .interpolate("basis")
        .x(function(d) { return x(d.date); })
        .y(function(d) { return y1(d.pressure); });
    // setup the svg area
    var svg = d3.select("#out")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    // Define the div for the tooltip
    var div = d3.select("body").append("div")
        .attr("class", "tooltip")
        .style("opacity", 0);

    // Get the data
    getToken('token.ini', function(){
        var url = "/api/v1/sensor/Klima?per_page=100";
        entries = {};
        getEntries(url,'klima', token, 300, function(){
        data = entries.klima;
        data.forEach(function(d) {
            d.date = +d.timestamp * 1000;
            d.temperature = +d.value['temperature[C]'];
            d.pressure = +d.value['pressure[hPa]'];
        });
        // Scale the range of the data
        x.domain(d3.extent(data, function(d) { return d.date; }));
        y0.domain([
            d3.min(data, function(d) {return Math.min(d.temperature); })-0.25,
            d3.max(data, function(d) {return Math.max(d.temperature); })+0.25]);
        y1.domain([
            d3.min(data, function(d) {return Math.min(d.pressure); })-0.25,
            d3.max(data, function(d) {return Math.max(d.pressure); })+0.25]);
        svg.append("path") // Add the temperature line.
            .style("stroke", "steelblue")
            .attr("d", temperatureLine(data));
        svg.selectAll("dot")
            .data(data)
            .enter().append("circle")
                .style('fill', 'steelblue')
                .style('stroke', 'lightsteelblue')
                .style('stroke-width', 0)
                .attr("r", 1.5)
                .attr("cx", function(d) { return x(d.date); })
                .attr("cy", function(d) { return y0(d.temperature); })
                .attr("id", function(d, i) { return "circle-" + i; })
                .on("mouseover", function(d, i) {
                    d3.select('#circle-'+ i)
                        .attr('r', 6)
                        .style('stroke-width', 7)
                        .style('opacity', 0.5);
                    div.transition()
                        .duration(200)
                        .style("opacity", 0.8);
                    div.html("<b>"+d.timestamp_rcf822 +"</b><br/>"  + d.temperature + " &deg;C")
                        .style('background', 'lightsteelblue')
                        .style("left", (d3.event.pageX) + "px")
                        .style("top", (d3.event.pageY - 28) + "px");
                })
                .on("mouseout", function(d, i) {
                    d3.select('#circle-'+ i)
                        .attr('r', 1.5)
                        .style('stroke-width', 0)
                        .style('opacity', 1);
                    div.transition()
                        .duration(500)
                        .style("opacity", 0);
                });
        // Add the pressure line.
        svg.append("path") // Add the pressure line.
            .style("stroke", "red")
            .attr("d", pressureLine(data));
        svg.selectAll("dot")
            .data(data)
            .enter().append("circle")
                .style('fill', 'red')
                .style('stroke', 'pink')
                .style('stroke-width', 0)
                .attr("r", 1.5)
                .attr("cx", function(d) { return x(d.date); })
                .attr("cy", function(d) { return y1(d.pressure); })
                .attr("id", function(d, i) { return "circlep-" + i; })
                .on("mouseover", function(d, i) {
                    d3.select('#circlep-'+ i)
                        .attr('r', 6)
                        .style('stroke-width', 7)
                        .style('opacity', 0.5);
                    div.transition()
                        .duration(200)
                        .style("opacity", 0.8);
                    div.html("<b>"+d.timestamp_rcf822 +"</b><br/>"  + d3.format('.2f')(d.pressure) + " mbar")
                        .style('background', 'pink')
                        .style("left", (d3.event.pageX) + "px")
                        .style("top", (d3.event.pageY - 28) + "px");
                })
                .on("mouseout", function(d, i) {
                    d3.select('#circlep-'+ i)
                        .attr('r', 1.5)
                        .style('stroke-width', 0)
                        .style('opacity', 1);
                    div.transition()
                        .duration(500)
                        .style("opacity", 0);
                });
        svg.append("g") // Add the X Axis
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .selectAll("text")
                .style("text-anchor", "end")
                .attr("dx", "-.8em")
                .attr("dy", ".15em")
                .attr("transform", function(d) {
                    return "rotate(-65)";
                });
        svg.append("g") // Add the temperature axis
            .attr("class", "y axis")
            .style("fill", "steelblue")
            .call(yAxisLeft);
        svg.append("g") // Add the pressure axis
            .attr("class", "y axis")
            .attr("transform", "translate(" + width + " ,0)")
            .style("fill", "red")
            .call(yAxisRight);
        svg.append("text") // Add the text label for the temperature axis
            .attr("transform", "rotate(-90)")
            .attr("x", 0)
            .attr("y", -40)
            .style("fill", "steelblue")
            .style("text-anchor", "end")
            .text("Temperature (Degrees Centigrade)");
        svg.append("text") // Add the text label for the pressure axis
            .attr("transform", "rotate(-90)")
            .attr("x", 0)
            .attr("y", width + 53)
            .style("fill", "red")
            .style("text-anchor", "end")
            .text("Pressure (millibars)");
        });
   });
            // wrangle the data into the correct formats and units
});
