function parseINIString(data) {
    var regex = {
        section: /^\s*\[\s*([^\]]*)\s*\]\s*$/,
        param: /^\s*([^=]+?)\s*=\s*(.*?)\s*$/,
        comment: /^\s*;.*$/
    };
    var value = {};
    var lines = data.split(/[\r\n]+/);
    var section = null;
    lines.forEach(function(line) {
        if(regex.comment.test(line)) {
            return;
        }else if(regex.param.test(line)) {
            var result = line.match(regex.param);
            if(section){
                value[section][result[1]] = result[2];
            }else{
                value[result[1]] = result[2];
            }
        }else if(regex.section.test(line)) {
            var result2 = line.match(regex.section);
            value[result2[1]] = {};
            section = result2[1];
        }else if(line.length === 0 && section) {
            section = null;
        }
    });
    return value;
}

var token = null;

function getToken(url, cb) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            token = parseINIString(this.responseText).Token.token;
            if (cb !== null)
                cb();
        }
    };
    xhttp.open('GET', url, true);
    xhttp.send();
}

var entries = {};

// url path to endpoint
// key: the key in entries for the result.
// count === null -> all
function getEntries(url, key, token, count, cb) {
    if (!(key in entries)) {
        entries[key] = [];
    }
    var xhttp = new XMLHttpRequest();
    xhttp.key = key;
    xhttp.cb = cb;

    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 ) {
            if (this.status == 200 ) {
                var data = JSON.parse(this.responseText);
                var dataentries = null;
                if ('sensors' in data)
                    dataentries = data.sensors;
                else
                    dataentries = data.values;
                if (count === null) {
                    entries[this.key] = entries[this.key].concat(dataentries);
                    if (data.paginate.links.next_page !== "")
                        getEntries(data.paginate.links.next_page, this.key, token, null, this.cb);
                    else
                        if (this.cb !== null)
                            this.cb(this.key);
                }
                else {
                    if (count > data.paginate.total)
                        count = data.paginate.total;
                    if (entries[this.key].length < count) {
                        if (entries[this.key].length + dataentries.length <= count)
                            entries[this.key] = entries[this.key].concat(dataentries);
                        else {
                            var i;
                            for (i in dataentries) {
                            var entry = dataentries[i];
                                entries[this.key].push(entry);
                                if (entries[this.key].length == count)
                                    break;
                            }
                        }
                    }
                    if (data.paginate.links.next_page !== "" && entries[this.key].length < count)
                        getEntries(data.paginate.links.next_page, this.key, token, count, this.cb);
                    else
                        if (this.cb !== null)
                            this.cb(this.key);
                }
            } else console.log(this.responseText);
        }
    };
    xhttp.open('GET', url, true);
    xhttp.setRequestHeader('Authorization', 'Bearer ' + token);
    xhttp.send();
}

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}
// vim: ts=4 sta sw=4 et ai
