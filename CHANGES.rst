=========
Changelog
=========

Version 0.1
===========

- first commit with token endpoint
- simple measurement endpoint with token auth
- model User + bugfixes
- REST-API documentation
- finished token + document it
- sensor endpoint finished
- check sensor_id format
- start doc for sensor endpoint
- unify http error messages
- docu
- tests
- add tests for token endpoint
- Correct link in paginate entrys
- new token in testfile
- install scripts + supervisord config
- move config file into app dir to use pytest as standalone
- pep8 tests
- add test to check encoding
- pylint complaint
- typo
- remove dead code
- reorganize tests + test sensor post
- correct test
- add test-requirement
- change the way the value is posted
- doc: Statuscode 201 for successful post
- some missing directories
- rename Table datapint to datapoint
- add unique index for sensor and timestamp
- Merge branch 'installer'
- typo in ignore file
- add requests example for logserver access
- pep8 typos
- Reorganize example
- add javascript example
- documentet each endpoint of the api
- improve javescrip example
- typo
- add helperscript which loads the environment variables befor executing the command in theargument
- add flask command to create a configfile
- use click for output in flask commands
- add flask command to change user privileges
- bump PyJWT version to 1.5.3
- create .env-file with reliable order
- reoder entries in env-file
- add pam module template
- simplify some commands
- doc: installation guide
- Bugfix: more then 10 entries + d3.js example
- dos2unix
- add Klima sensor (Bme280)
- interpolate values on klima.html (-> smoother curves)
- improve klima.html
- format some files

