"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        flasky.py

    FIRST RELEASE
        2017-07-21  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import binascii
import os

import click

from app import create_app, db
from app.models import User, Permission, DataPoint, Token
from flask_migrate import Migrate


basedir = os.path.abspath(os.path.dirname(__file__))
app = create_app(os.getenv('LOGSERVER_CONFIG') or 'development')
migrate = Migrate(app, db)


@app.shell_context_processor
def make_shell_context():
    """inject classes to shell content"""
    return dict(app=app, db=db, User=User, Permission=Permission,
                DataPoint=DataPoint, Token=Token)


@app.cli.command()
@click.option('--force/--no-force', default=False,
              help='create .env-file even if it already exists.')
def create_env(force=False):
    """Creates the .env-file file with default values and random secret keys"""
    envfile = os.path.join(basedir, '.env-file')
    if os.path.exists(envfile):
        if force:
            click.echo('Do you want to overwrite the existing file? [N/y] ',
                       nl=False)
            query = click.getchar()
            if query != 'y':
                click.secho('Abort', fg='red')
                return
        else:
            click.secho(('File %s already exists. Use --force if you want to '
                         'overwrite it.') % envfile, fg='yellow')
            return
    data = [
        ('SECRET_KEY', binascii.hexlify(os.urandom(24)).decode()),
        ('JWT_SECRET_KEY', binascii.hexlify(os.urandom(24)).decode()),
        ('LOGSERVER_CONFIG', 'production'),
        ('DATABASE_URL', 'sqlite:////home/logserver/db.sqlite'),
        # ('LOGSERVER_ADMIN', '  # user1:user2:...'),
        ('PAM_MODUL', 'common-auth'),
        ('URL_PREFIX', '/api')
    ]
    with open(envfile, 'w') as env:
        env.write('\n'.join('='.join(entry) for entry in data))
    click.secho('File %s successfully written.' % basedir, fg='green')


@app.cli.command()
@click.argument('user')
@click.option('--promote', 'promote', flag_value=True, default=None,
              help='promote user to admin')
@click.option('--demote', 'promote', flag_value=False, default=None,
              help='demote user to normal user')
def adminuser(user, promote):
    """Check user for admin privileges and set/unset these."""
    user_db = User.query.filter_by(username=user).first()
    if user_db is None:
        click.secho('User %s doesn\'t exists' % user, fg='red')
        return
    if promote is None:
        print(user_db.permission)
        if user_db.is_administrator():
            click.secho('User %s is an administrator' % user, bg='green',
                        fg='black')
        else:
            click.secho('User %s is a normal user' % user, bg='blue',
                        fg='black')
    else:
        if promote:
            user_db.set_permission(Permission.ADMINISTRATOR)
            click.secho('User %s is now an administrator' % user, fg='green')
        else:
            user_db.unset_permission(Permission.ADMINISTRATOR)
            print(user_db.permission)
            user_db.set_permission(Permission.GRANT_GET)
            print(user_db.permission)
            user_db.set_permission(Permission.GRANT_POST)
            print(user_db.permission)
            click.secho('User %s is now a normal user' % user, fg='blue')
        db.session.add(user_db)
        db.session.commit()
# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
