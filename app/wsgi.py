"""
 COPYRIGHT (C) 2017 by Sebastian Stigler

 NAME

 FIRST RELEASE
    2017-07-21  Sebastian Stigler  sebastian.stigler@hs-aalen.de
"""
import os

from . import create_app

# Create an application instance that web servers can use. We store it as
# "application" (the wsgi default) and also the much shorter and convenient
# "app".
application = app = create_app(
    os.environ.get('LOGSERVER_CONFIG', 'production'))

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
