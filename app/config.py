"""
    COPYRIGHT (C) 2016 by Sebastian Stigler

    NAME
        config.py -- Cofiguration of the webapp

    FIRST RELEASE
        2016-09-15  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""

import os

basedir = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..')


class Config(object):
    """Basic Configuration"""
    TESTING = False
    DEBUG = False
    SECRET_KEY = os.environ.get(
        'SECRET_KEY', '9970abe4cff74148b5a49ae151da7af33e10332073dd937a')
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DATABASE_URL', 'sqlite:///' + os.path.join(basedir, 'db.sqlite'))
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    LOGSERVER_ADMIN = os.environ.get('LOGSERVER_ADMIN', '')
    PAM_MODUL = os.environ.get('PAM_MODUL', 'common-auth')
    URL_PREFIX = os.environ.get('URL_PREFIX', '/api')
    JWT_SECRET_KEY = os.environ.get(
        'JWT_SECRET_KEY', 'ae53748d8fc62f135b93d006863ae7dc72d4fb35b262dcb5')


class DevelopmentConfig(Config):
    """Configuration for Development Server"""
    DEBUG = True


class ProductionConfig(Config):
    """Configuration for Production Server"""
    pass


class TestingConfig(Config):
    """Configuration for Testing Server"""
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SECRET_KEY = 'SECRET'
    JWT_SECRET_KEY = 'SECRET'
    LOGSERVER_ADMIN = 'ADMIN'

config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
