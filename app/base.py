"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        base.py

    FIRST RELEASE
        2017-07-21  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import inspect
import re

from flask import abort, g
from flask_restful import Resource, reqparse


extra_errors = {
    'Forbidden': {
        'message': "You don't have the permission to access the requested "
                   'resource. It is either read-protected or not readable by '
                   'the server.',
        'status': 403},
    'BadRequest': {
        'message': 'The browser (or proxy) sent a request that this server '
                   'could not understand.',
        'status': 400},
    'NotAcceptable': {
        'message': 'Token is not valid for the payload of this request.',
        'status': 406},
    'CONFLICT': {
        'message': 'Timestamp conflict. '
                   'Only one value per seconde and sensor.',
        'status': 409},
    'PreconditionFailed': {
        'message': 'The user has not enough privileges for this request.',
        'status': 412}
}


class BaseResource(Resource):
    """Basic class"""

    def delete(self, *args, **kwargs):  # pylint:disable=R0201,W0613
        """HTML method"""
        abort(405)

    def get(self, *args, **kwargs):  # pylint:disable=R0201,W0613
        """HTML method"""
        abort(405)

    def patch(self, *args, **kwargs):  # pylint:disable=R0201,W0613
        """HTML method"""
        abort(405)

    def post(self, *args, **kwargs):  # pylint:disable=R0201,W0613
        """HTML method"""
        abort(405)

    def put(self, *args, **kwargs):  # pylint:disable=R0201,W0613
        """HTML method"""
        abort(405)


def verify_data(arguments, check_hwid=False):
    """verify if the requested payload is allowed by the token

    Args:
       arguments - is a dict or a RequestParser object
       check_hwid - add the hwid to checked properties

    Raises:
        HTTPError 406
        set the (parsed) arguments to g.args
    """
    if 'jwt_claims' not in g:  # pragma: no cover
        # no token_auth.login_required used
        return
    if isinstance(arguments, reqparse.RequestParser):
        g.args = arguments.parse_args()
    elif isinstance(arguments, str) and not check_hwid:
        g.args = {'sensor': arguments}
    else:
        g.args = arguments

    # get the name of the caller of this function
    method = inspect.stack()[1][3].upper()
    try:
        check = [method in g.jwt_claims['mtds']]
        if g.args is not None:
            if 'uni' in g.jwt_claims and g.jwt_claims and method == 'GET':
                pattern = r'^[a-zA-Z0-9_]+$'
                check.append(re.match(pattern, g.args['sensor']) is not None)
            else:
                check.append(g.args['sensor'] in g.jwt_claims['ids'])
        if check_hwid:
            check.append(g.args['hwid'] == g.jwt_claims['hwid'])
        if not all(check):
            abort(406)
    except LookupError:  # pragma: no cover
        abort(406)

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
