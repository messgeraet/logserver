"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        tokenutils.py

    DESCRIPTION
        This module contains functions to generate and verify JWT tokens.

    FIRST RELEASE
        2017-07-21  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import re

from datetime import datetime, timedelta

from flask import current_app, g, abort
from flask_httpauth import HTTPTokenAuth, HTTPBasicAuth
import jwt

from app import db
from app.models import User, Permission

token_auth = HTTPTokenAuth('Bearer')


def generate_token(hwid, ids, mtds, exp, user, test_exp=False, uni=False):
    """Generate a JWT token.
    :param hwid uuid of the machine which requests the token
    :param ids a list of sensor_ids for which the token is valid
    :param mtds a list of http methods for which the token is valid
    :param exp expiration time in hours
    :param user who created the token
    :param test_exp allow invalid exps for testing purpose
    :param uni token is valid for all ids in the database but only for GET
    """
    secret_key = current_app.config['JWT_SECRET_KEY']
    payload = {
        'hwid': hwid,
        'ids': ids,
        'mtds': mtds,
        'exp': datetime.utcnow() + timedelta(hours=exp),
        'iss': user.username}
    pattern = r'^[a-zA-Z0-9_]+$'
    if uni:
        if not user.can(Permission.GRANT_UNIVERSAL_GET):
            abort(412)
        payload['ids'] = ['*']
        payload['mtds'] = ['GET']
        payload['uni'] = True
    else:
        for sensor_id in ids:
            if re.match(pattern, sensor_id) is None:
                abort(400)
    for method in payload['mtds']:
        permission = 'GRANT_' + method
        if not hasattr(Permission, permission):
            abort(412)
        if not user.can(getattr(Permission, permission)):
            abort(412)
    if exp <= 0 and not test_exp:
        if not user.can(Permission.GRANT_NOT_EXPIRED):
            abort(412)
        del payload['exp']
    return jwt.encode(payload, secret_key, algorithm='HS512').decode('utf-8')


@token_auth.verify_token
def verify_token(token):
    """Token verification callback."""

    secret_key = current_app.config['JWT_SECRET_KEY']
    g.jwt_claims = {}
    try:
        g.jwt_claims = jwt.decode(token, secret_key, algorithms=['HS512'])
        g.jwt_token = token
    except:  # pylint:disable=W0702
        # we really don't care what is the error here, any tokens that do not
        # pass validation are rejected
        return False
    return True


@token_auth.error_handler
def token_error():
    """Return a 403 error to the client."""
    abort(403)


basic_auth = HTTPBasicAuth()


def login_logger(func):
    """decorator"""
    def wrapper(*args):
        """wrapper"""
        ret = func(*args)
        msg = 'Login attempt from "{0}" was {1}successfull'
        current_app.logger.info(msg.format(args[0], '' if ret else 'not '))
        return ret
    return wrapper


@basic_auth.verify_password
@login_logger
def verify_password(username, password):
    """Password verification callback."""
    user = User.query.filter_by(username=username).first()
    if user is None:
        user = User.create({'username': username})
    if not user.verify_password(password):
        return False
    db.session.add(user)
    db.session.commit()
    g.current_user = user
    return True


@basic_auth.error_handler
def unauthorized():
    """errorhandler"""
    # return 403 instead of 401 to prevent browsers from displaying the default
    # auth dialog
    abort(403)
# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
