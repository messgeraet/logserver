"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        models.py

    FIRST RELEASE
        2017-07-25  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import json

from base64 import b64encode, b64decode
from time import time
from zlib import compress, decompress

import pam
import jwt

from sqlalchemy.exc import IntegrityError

from flask import abort, current_app

from app import db


def timestamp():
    """Return the current timestamp as an integer."""
    return int(time())


class Permission:
    """Permissions"""
    GRANT_NOT_EXPIRED = 0x01
    GRANT_GET = 0x02
    GRANT_POST = 0x04
    GRANT_DELETE = 0x08
    GRANT_PATCH = 0x10
    GRANT_PUT = 0x20
    GRANT_UNIVERSAL_GET = 0x40
    ADMINISTRATOR = 0xff


class User(db.Model):
    """User model"""
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)  # pylint: disable=C0103
    username = db.Column(db.String(32), nullable=False, unique=True)
    permission = db.Column(
        db.Integer,
        default=(Permission.GRANT_POST | Permission.GRANT_GET)
        )
    is_active = db.Column(db.Boolean, default=True)
    created_at = db.Column(db.Integer, default=timestamp)
    updated_at = db.Column(db.Integer, default=timestamp, onupdate=timestamp)

    def can(self, permission):
        """Check for permission"""
        return self.permission & permission == permission

    def set_permission(self, permission):  # pragma: no cover
        """set permission"""
        self.permission |= permission & 0xff

    def unset_permission(self, permission):  # pragma: no cover
        """unset permission"""
        self.permission &= (permission ^ 0xff) & 0xff

    def is_administrator(self):
        """Check for administrator"""
        return self.can(Permission.ADMINISTRATOR)

    def verify_password(self, password):  # pragma: no cover
        """use pam to login"""
        if not self.is_active and self.id is not None:
            return False
        if current_app.config['TESTING']:
            return password == 'PASSWORD'
        pam_h = pam.pam()
        service = current_app.config['PAM_MODUL']
        return pam_h.authenticate(self.username, password, service=service)

    @staticmethod
    def create(data):
        """Create a new user."""
        user = User()
        user.from_dict(data, partial_update=False)
        if user.username in current_app.config['LOGSERVER_ADMIN'].split(':'):
            user.permission = Permission.ADMINISTRATOR
        return user

    def from_dict(self, data, partial_update=True):
        """Import user data from a dictionary."""
        for field in ['username']:
            try:
                setattr(self, field, data[field])
            except KeyError:  # pragma: no cover
                if not partial_update:
                    abort(400)

    def to_dict(self):  # pragma: no cover
        """Export user to a dictionary."""
        return {
            'id': self.id,
            'username': self.username,
            'permission': self.permission,
            'is_active': self.is_active,
            'created_at': self.created_at,
            'updated_at': self.updated_at,
            }

    @staticmethod
    def create_testusers():
        """Testuser creation"""
        if not User.query.all():
            testuser = User.create({'username': 'testuser'})
            adminuser = User.create({'username': 'testadminuser'})
            adminuser.permission = Permission.ADMINISTRATOR
            db.session.add(testuser)
            db.session.add(adminuser)
            db.session.commit()


class Base64():
    """Discriptor to decode and encode a base64 encodede column"""

    def __init__(self, field):
        self.field = field

    def __get__(self, instance, owner):
        """return readable entry"""
        value = getattr(instance, self.field)
        if value is None:  # pragma: no cover
            return None
        return decompress(b64decode(value.encode('utf-8'))).decode('utf-8')

    def __set__(self, instance, data):
        """store column as base64 so that dbimport/dbexport won't make any
           problems"""
        if data is not None:
            value = b64encode(compress(data.encode('utf-8'))).decode('utf-8')
            setattr(instance, self.field, value)


class Token(db.Model):
    """Tokens Model"""
    __tablename__ = 'tokens'
    id = db.Column(db.Integer, primary_key=True)  # pylint: disable=C0103
    token = db.Column(db.String, nullable=False, unique=True)
    timestamp = db.Column(db.Integer, default=timestamp)

    datapoints = db.relationship('DataPoint', backref='token')

    @staticmethod
    def create(data):
        """create new token"""
        token = Token()
        token.from_dict(data, partial_update=False)
        return token

    def from_dict(self, data, partial_update=True):
        """Import template data from a dictionary."""
        for field in ['token']:
            try:
                setattr(self, field, data[field])
            except KeyError:  # pragma: no cover
                if not partial_update:
                    abort(400)

    def to_dict(self, value_decode=False):  # pragma: no cover
        """Export token to a dictionary."""
        token = {
            'id': self.id,
            'token': self.sensor,
            'timestamp': self.timestamp
        }
        if value_decode:
            token['payload'] = jwt.decode(self.token, verify=False)
        return token


class DataPoint(db.Model):
    """DataPoint Model"""
    __tablename__ = 'datapoints'

    id = db.Column(db.Integer, primary_key=True)  # pylint: disable=C0103
    sensor = db.Column(db.String, index=True, nullable=False)
    value_base64 = db.Column(db.String, nullable=False)
    value = Base64('value_base64')
    token_id = db.Column(db.Integer, db.ForeignKey('tokens.id'))
    timestamp = db.Column(db.Integer, default=timestamp)

    @staticmethod
    def create(data):
        """create new datapoint"""
        datapoint = DataPoint()
        datapoint.from_dict(data, partial_update=False)
        return datapoint

    def from_dict(self, data, partial_update=True):
        """Import template data from a dictionary."""
        for field in ['sensor', 'value', 'token_id']:
            try:
                setattr(self, field, data[field])
            except KeyError:  # pragma: no cover
                if not partial_update:
                    abort(400)

    def to_dict(self, value_decode=False, with_token=False):
        """Export datapoint to a dictionary."""
        datapoint = {
            'id': self.id,
            'sensor': self.sensor,
            'value': self.value,
            'timestamp': self.timestamp
        }
        if value_decode:
            datapoint['value'] = json.loads(self.value)
        if with_token:  # pragma: no cover
            datapoint['token'] = jwt.decode(self.token, verify=False)
        return datapoint

    @staticmethod
    def create_testdatapoints():
        """Testdatapoint creation"""
        if not DataPoint.query.all():
            from app.tokenutils import generate_token
            from random import random
            user = User.query.filter_by(username='testuser').first()
            sensor_ids = ['test1', 'test2', 'test3']
            methods = ['GET', 'POST']
            token = generate_token('HARDWAREID', sensor_ids, methods, 1, user)
            data = {'token': token}
            dbtoken = Token.create(data)
            try:
                db.session.add(dbtoken)
                db.session.commit()
            except IntegrityError:  # pragma: no cover
                db.session.rollback()
            data['token_id'] = Token.query.filter_by(token=token).first().id

            mytimestamp = timestamp() - 99
            for sensor_id in sensor_ids:
                data['sensor'] = sensor_id
                for _ in range(33):
                    data['value'] = json.dumps({'sensor_value': random()})
                    datapoint = DataPoint.create(data)
                    datapoint.timestamp = mytimestamp
                    mytimestamp += 1
                    db.session.add(datapoint)
            db.session.commit()


db.Index(
    'ix_datapoints_sensor_timstamp',
    DataPoint.sensor, DataPoint.timestamp, unique=True)

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
