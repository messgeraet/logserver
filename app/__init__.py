"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        __init__.py


    FIRST RELEASE
        2017-07-21  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import os

import flask_restful
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from app.config import config

db = SQLAlchemy()


def create_app(environment='development'):
    """create the app object"""
    app = Flask(__name__)

    app.config.from_object(config[environment])

    db.init_app(app)

    from app.api_1_0 import API_VERSION
    from app.api_1_0.token import token, token_bp  # pylint:disable=W0612
    app.register_blueprint(
        token_bp,
        url_prefix='{prefix}/{version}'.format(
            prefix=app.config['URL_PREFIX'],
            version=API_VERSION))

    from app.api_1_0.sensor import sensor, sensor_bp  # pylint:disable=W0612
    app.register_blueprint(
        sensor_bp,
        url_prefix='{prefix}/{version}'.format(
            prefix=app.config['URL_PREFIX'],
            version=API_VERSION))

    return app

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
