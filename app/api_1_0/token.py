"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        token.py

    FIRST RELEASE
        2017-07-21  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
from flask import Blueprint, g
import flask_restful
from flask_restful import reqparse

from sqlalchemy.exc import IntegrityError

from app import db
from app.base import BaseResource, extra_errors
from app.models import Token as DbToken
from app.tokenutils import generate_token, basic_auth

token_bp = Blueprint('token', __name__)
token = flask_restful.Api(token_bp, catch_all_404s=True, errors=extra_errors)


parser = reqparse.RequestParser()
parser.add_argument('hwid', required=True)
parser.add_argument('ids', action='append', required=True)
parser.add_argument('mtds', action='append', required=True)
parser.add_argument('exp', type=int, required=True)
parser.add_argument('uni', type=bool, required=False)


class Token(BaseResource):
    """Token"""
    decorators = [basic_auth.login_required]

    def post(self):  # pylint:disable=W0221
        args = parser.parse_args(strict=True)
        args['user'] = g.current_user
        data = {'token': generate_token(**args)}
        token = DbToken.create(data)
        try:
            db.session.add(token)
            db.session.commit()
        except IntegrityError:  # pragma: no cover
            db.session.rollback()

        return data, 201


token.add_resource(Token, '/token')

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
