"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        sensor.py

    FIRST RELEASE
        2017-07-24  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""
import json

from collections import OrderedDict
from datetime import datetime

from flask import Blueprint, g, abort
import flask_restful
from flask_restful import reqparse, fields, marshal, marshal_with
from flask_restful.fields import (
    to_marshallable_type, urlparse, url_for, urlunparse, MarshallingException,
    get_value, request
)

from sqlalchemy.exc import IntegrityError

from app import db
from app.base import BaseResource, extra_errors, verify_data
from app.models import DataPoint, Token
from app.tokenutils import token_auth


sensor_bp = Blueprint('sensor', __name__)


post_parser = reqparse.RequestParser()
post_parser.add_argument('sensor', required=True, location='json')
post_parser.add_argument('value', required=True, location='json')
post_parser.add_argument('hwid', required=True, location='json')
post_parser.add_argument('value', required=True, location='json')

paginat_parser = reqparse.RequestParser()
paginat_parser.add_argument('page', type=int, location='args', default=1)
paginat_parser.add_argument('per_page', type=int, location='args', default=10)


class ToTimestring(fields.Raw):
    """ToTimesting field"""
    def format(self, value):
        date = datetime.fromtimestamp(value)
        return date.strftime('%a, %d %b %y %H:%M:%S %z')


class ToJson(fields.Raw):
    """ToJson field"""
    def format(self, value):
        return json.loads(value)


class UrlExt(fields.Url):
    """UrlExt field"""
    def __init__(self, *args, **kwargs):
        if 'query' in kwargs:
            self.query = kwargs.pop('query')
        else:  # pragma: no cover
            self.query = []
        # 0: current, -1: previous 1: next page
        if 'page' in kwargs and kwargs['page'] in [-1, 0, 1]:
            self.page = kwargs.pop('page')
        else:
            self.page = 0
        super().__init__(*args, **kwargs)

    def output(self, key, obj):
        try:
            data = to_marshallable_type(obj)
            endpoint = self.endpoint if self.endpoint is not None \
                else request.endpoint
            ost = urlparse(url_for(endpoint, _external=self.absolute, **data))
            oquery = sorted(ost.query.split('&'))
            querylist = (que for que in oquery
                         if que.split('=')[0] in self.query)
            newpage = None
            if self.page == -1:
                newpage = get_value('prev_num', obj)
                if newpage is None:
                    return ""
            if self.page == 1:
                newpage = get_value('next_num', obj)
                if newpage is None:
                    return ""
            if newpage:
                querylist_new = [que for que in querylist
                                 if not que.startswith('page=')]
                querylist_new.append('page=%d' % newpage)
                querylist = sorted(querylist_new)
            query = '&'.join(querylist)
            if self.absolute:
                scheme = self.scheme if self.scheme is not None else ost.scheme
                return urlunparse(
                    (scheme, ost.netloc, ost.path, "", query, ""))
            return urlunparse(  # pragma: no cover
                ("", "", ost.path, "", query, ""))

        except TypeError as err:  # pragma: no cover
            raise MarshallingException(err)

paginatelinks_sensors_fields = OrderedDict([
    ('next_page', UrlExt(
        'sensor.sensor', query=['page', 'per_page'], absolute=True, page=1)),
    ('prev_page', UrlExt(
        'sensor.sensor', query=['page', 'per_page'], absolute=True, page=-1)),
])

paginate_sensors_fields = OrderedDict([
    ('page', fields.Integer),
    ('per_page', fields.Integer),
    ('pages', fields.Integer),
    ('total', fields.Integer),
    ('links', fields.Nested(paginatelinks_sensors_fields)),
])

sensors_fields = OrderedDict([
    ('sensors', fields.Raw),
    ('paginate', fields.Nested(paginate_sensors_fields)),
    ('uri', UrlExt(
        'sensor.sensor', query=['page', 'per_page'], absolute=True)),
])

sensor_fields = OrderedDict([
    ('sensor', fields.String),
    ('value', ToJson),
    ('timestamp', fields.Integer),
    ('timestamp_rcf822', ToTimestring(attribute='timestamp')),
    ('uri', fields.Url('sensor.sensorgetvalue', absolute=True)),
])

paginatelinks_sensorlist_fields = OrderedDict([
    ('next_page', UrlExt(
        'sensor.sensorgetlist', query=['page', 'per_page'],
        absolute=True, page=1)),
    ('prev_page', UrlExt(
        'sensor.sensorgetlist', query=['page', 'per_page'],
        absolute=True, page=-1)),
])

paginate_sensorlist_fields = OrderedDict([
    ('page', fields.Integer),
    ('per_page', fields.Integer),
    ('pages', fields.Integer),
    ('total', fields.Integer),
    ('links', fields.Nested(paginatelinks_sensorlist_fields)),
])

sensorlist_fields = OrderedDict([
    ('values', fields.Raw),
    ('paginate', fields.Nested(paginate_sensorlist_fields)),
    ('uri', UrlExt(
        'sensor.sensorgetlist', query=['page', 'per_page'], absolute=True)),
])


class Sensor(BaseResource):
    """Sensor store data and get sensornames"""
    decorators = [token_auth.login_required]

    def post(self):  # pylint:disable=W0221
        verify_data(post_parser, True)
        data = g.args.copy()
        data['token'] = g.jwt_token
        data['value'] = json.dumps(request.get_json(force=True)['value'])
        data['token_id'] = Token.query.filter_by(token=g.jwt_token).first().id
        datapoint = DataPoint.create(data)
        try:
            db.session.add(datapoint)

            db.session.commit()
        except IntegrityError:
            db.session.rollback()
            abort(409)
        return {'message': 'value stored'}, 201

    @marshal_with(sensors_fields)
    def get(self):  # pylint:disable=W0221
        verify_data(None)
        args = paginat_parser.parse_args()
        args['per_page'] = max(min(args['per_page'], 100), 1)

        queryobj = DataPoint.sensor.distinct().label('sensor')
        base = db.session.query(queryobj)  # pylint:disable=W0612
        if 'uni' in g.jwt_claims and g.jwt_claims['uni']:
            values = base
        else:
            # A very ugly hack:
            filtercondition = ('(DataPoint.sensor == "%s")' % id_
                               for id_ in g.jwt_claims['ids'])
            cmd = 'base.filter(%s)' % ('|'.join(filtercondition))
            values = eval(cmd)  # pylint:disable=W0123
        ordered = values.order_by(DataPoint.sensor)
        paginated = ordered.paginate(args['page'], args['per_page'])
        marshaled = [res.sensor for res in paginated.items]

        result = {
            'page': paginated.page,
            'per_page': paginated.per_page,
            'sensors': marshaled,
            'paginate': {
                'page': paginated.page,
                'per_page': paginated.per_page,
                'pages': paginated.pages,
                'total': paginated.total,
                'links': {
                    'page': paginated.page,
                    'per_page': paginated.per_page,
                    'prev_num': paginated.prev_num,
                    'next_num': paginated.next_num,
                }
            }
        }
        return result


class SensorGetList(BaseResource):
    """Values for one sensorname"""

    decorators = [token_auth.login_required]

    @marshal_with(sensorlist_fields)
    def get(self, sensor):  # pylint:disable=W0221
        verify_data(sensor)
        args = paginat_parser.parse_args()
        args['per_page'] = max(min(args['per_page'], 100), 1)

        values = DataPoint.query.filter_by(sensor=sensor)
        ordered = values.order_by(DataPoint.timestamp.desc())
        paginated = ordered.paginate(args['page'], args['per_page'])
        marshaled = [marshal(res.to_dict(), sensor_fields)
                     for res in paginated.items]

        result = {
            'sensor': sensor,
            'page': paginated.page,
            'per_page': paginated.per_page,
            'values': marshaled,
            'paginate': {
                'sensor': sensor,
                'page': paginated.page,
                'per_page': paginated.per_page,
                'pages': paginated.pages,
                'total': paginated.total,
                'links': {
                    'sensor': sensor,
                    'page': paginated.page,
                    'per_page': paginated.per_page,
                    'prev_num': paginated.prev_num,
                    'next_num': paginated.next_num,
                }
            }
        }
        return result


class SensorGetValue(BaseResource):
    """Value for one measurement"""
    decorators = [token_auth.login_required]

    @marshal_with(sensor_fields)
    def get(self, sensor, timestamp):  # pylint:disable=W0221
        verify_data(sensor)
        if not 0 <= timestamp <= 9999999999:
            abort(404)
        result = DataPoint.query.filter_by(
            sensor=sensor, timestamp=timestamp).first_or_404()
        result.to_dict(value_decode=True)
        return result


sensor = flask_restful.Api(sensor_bp, catch_all_404s=True, errors=extra_errors)
sensor.add_resource(Sensor, '/sensor')
sensor.add_resource(SensorGetList, '/sensor/<string:sensor>')
sensor.add_resource(SensorGetValue, '/sensor/<string:sensor>/<int:timestamp>')

# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
