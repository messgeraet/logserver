"""
    COPYRIGHT (C) 2017 by Sebastian Stigler

    NAME
        __init__.py

    FIRST RELEASE
        2017-07-21  Sebastian Stigler  sebastian.stigler@hs-aalen.de

"""

API_VERSION = 'v1'


# vim: ft=python ts=4 sta sw=4 et ai
# python: 3
